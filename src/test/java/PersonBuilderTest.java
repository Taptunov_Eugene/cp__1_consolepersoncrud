import com.company.Person;
import com.company.PersonBuilder;
import com.company.PersonStorage;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class PersonBuilderTest {
    @Test
    public void createPersonTest(){
        PersonStorage personStorage = new PersonStorage(new ArrayList<Person>());
        Person expected = new Person(0,"vlad", "lutsenko", 20,"kyiv");
        Person actual = PersonBuilder.createPerson(personStorage,"vlad",
                "lutsenko", 20,"kyiv");
        assertEquals(expected,actual);
    }
}
